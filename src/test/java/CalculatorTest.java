import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
    public int Add(int x, int y) {
        return x + y;
    }

    public int Rem(int x, int y) {
        return x - y;
    }

    @Test
    @DisplayName("CYKA BLIAT")
    public void checkRemTest() {
        Assertions.assertEquals(Rem(10,4), 6);
    }

    @Test
    @DisplayName("IDI NA HUJ")
    public void checkAddTest() {
        Assertions.assertEquals(Add(3,5), 8);
    }

}
